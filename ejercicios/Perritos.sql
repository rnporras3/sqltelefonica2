""1er ejercicio relacion uno a muchos""
          ""Ricardo Porras""
"" tablas pensadas desde una base de datos de paseadores de perros""


create table raza(
RAZA_ID integer NOT NULL PRIMARY KEY(RAZA_ID) IDENTITY, 
RAZA_DESCRIPCION VARCHAR(60)
);

create table perritos(
NOM_ID integer NOT NULL PRIMARY KEY(NOM_ID) IDENTITY,
RAZA_ID integer FOREIGN KEY REFERENCES raza(RAZA_ID),
NOM_DESCRIPCION VARCHAR(50) 
);


